# EMPRESAS-WEB-IOASYS

Esse projeto contém uma aplicação em ReactJS que faz requisições para uma API externa. O login é feito nos padrões OAuth 2.0, também são feitas requisições para busca e filtragem de dados.

## Como usar

Baixe este repositório e na pasta raíz do projeto execute o seguinte comando para instalar as depedências:

### `npm install`

Então, execute o seguinte comando para iniciar a aplicação no navegador:

### `npm start`

A aplicação será iniciada no endereço:

### `http://localhost:3000`

Recomendo que seja utilizado o navegação anônima para utilização da aplicação, para garantir que o armazenamento local estará limpo.

## Bibliotecas utilizadas

- react-fontawesome: lib utilizada para facilitar o uso de alguns ícones.
- bootstrap e react-bootstrap: libs utilizadas para criação da interface responsiva.
- axios: lib utilizada como cliente HTTP da aplicação, usei para fazer requisições a API externa.
- react-router-dom: lib utilizada para a administração das rotas da aplicação.
