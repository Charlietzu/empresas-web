/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import "./login.css";
import logo from "../../assets/img/logo-home.png";
import { getLoginHeaders } from "../../api/authApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleNotch } from "@fortawesome/free-solid-svg-icons";

import emailIco from "../../assets/svg/ic-email.svg";
import passwordIco from "../../assets/svg/ic-cadeado.svg";
import eyeIco from "../../assets/svg/eye.svg";

/**
 * Componente stateless da página de Login
 * @param {method} handleLogin
 * @param {method} handleEyeClick
 * @param {state} loading
 * @param {state} inputType
 */
const LoginPage = ({
  handleLogin,
  loading,
  inputType,
  handleEyeClick,
  invalidLogin,
}) => {
  return (
    <div className="row text-center loginContainer">
      <div className="col"></div>
      <div className="col-8 col-sm-4  align-self-center">
        <form onSubmit={handleLogin} id="login">
          <img src={logo} />
          {getLoginHeaders() ? (
            <h4 className="pt-4">
              Você já está logado! Clique <a href="/home">aqui</a> para voltar a
              página inicial.
            </h4>
          ) : loading ? (
            <FontAwesomeIcon
              icon={faCircleNotch}
              spin
              className="fa-5x row mx-auto mt-5 loadingIco"
            />
          ) : (
            <>
              <h4 className="pt-4" style={{ fontWeight: "bold" }}>
                BEM VINDO AO EMPRESAS
              </h4>
              <p>
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
                accumsan.
              </p>
              <div
                className={
                  invalidLogin
                    ? "form-group input-block inputError"
                    : "form-group input-block"
                }
              >
                <img src={emailIco} />
                <input
                  type="email"
                  className="form-control inputField"
                  placeholder="Enter email"
                  name="email"
                  required
                />
              </div>
              <div
                className={
                  invalidLogin
                    ? "form-group input-block inputError"
                    : "form-group input-block"
                }
              >
                <img src={passwordIco} />
                <input
                  type={inputType}
                  className="form-control inputField"
                  placeholder="Password"
                  name="password"
                  required
                />
                <img
                  src={eyeIco}
                  className="passwordEye"
                  onClick={handleEyeClick}
                />
              </div>
              {invalidLogin ? (
                <p className="invalidLogin">
                  Credenciais informadas são inválidas, tente novamente.
                </p>
              ) : (
                <></>
              )}
              <button type="submit" className="btn btn-block btnLogar">
                Entrar
              </button>
            </>
          )}
        </form>
      </div>
      <div className="col"></div>
    </div>
  );
};

export default LoginPage;
