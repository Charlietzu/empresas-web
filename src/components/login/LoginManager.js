import React, { useState } from "react";
import LoginPage from "./LoginPage";
import apiInstance from "../../api/empresasApi";
import { setLogin } from "../../api/authApi";
import { useHistory } from "react-router-dom";

/**
 * Componente administrador de estado da página de Login
 */
export function LoginManager() {
  //State para definir se o ícone de login será renderizado.
  const [loading, setLoading] = useState(false);
  //State para definir o input type do input de senha.
  const [inputType, setInputType] = useState("password");
  //State para definir se o ocorreu algum erro no login.
  const [invalidLogin, setInvalidLogin] = useState(false);

  let history = useHistory();

  /**
   * Método para fazer a requisição de login.
   *
   * Ela define o state loading para verdadeiro caso a requisição esteja sendo feita,
   * se ela concluir ou der erro, o loading é definido para false.
   *
   * Fazemos um POST para a instância do axios um body com email e senha na requisição.
   * Caso a requisição seja concluída com sucesso, chamamos a função setLogin da API
   * de autorização para salvar os headers e então redirecionamos o usuário para a home.
   */
  function handleLogin(event) {
    event.preventDefault();
    let signInURL = "/users/auth/sign_in";
    setLoading(true);
    apiInstance
      .post(
        signInURL,
        {
          email: event.target.email.value,
          password: event.target.password.value,
        },
        {
          headers: { "Content-Type": "application/json" },
        }
      )
      .then((response) => {
        setLogin(
          response.headers["access-token"],
          response.headers.client,
          response.headers.uid
        );
        setInvalidLogin(false);
        history.push("/home");
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setInvalidLogin(true);
        setLoading(false);
      });
  }

  /**
   * Método para definir se a senha será mostrada ou oculta.
   */
  function handleEyeClick() {
    inputType === "password" ? setInputType("text") : setInputType("password");
  }

  return (
    <LoginPage
      handleLogin={handleLogin}
      loading={loading}
      inputType={inputType}
      handleEyeClick={handleEyeClick}
      invalidLogin={invalidLogin}
    />
  );
}

export default LoginManager;
