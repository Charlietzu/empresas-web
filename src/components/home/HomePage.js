/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import "./home.css";
import logo from "../../assets/img/logo-nav.png";
import searchIco from "../../assets/svg/search.svg";
import Enterprises from "./enterprise/Enterprises";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faCircleNotch } from "@fortawesome/free-solid-svg-icons";

/**
 * Componente stateless da página Home.
 * @param {method} handleSearch
 * @param {method} handleOnInfo
 * @param {method} handleSearchClick
 * @param {method} getEnterpriseName
 * @param {state} enterprises
 * @param {state} onInfo
 * @param {state} searchActive
 * @param {state} enterpriseName
 *
 */
const HomePage = ({
  handleSearch,
  enterprises,
  handleOnInfo,
  onInfo,
  firstSearch,
  loading,
  handleSearchClick,
  searchActive,
  enterpriseName,
  getEnterpriseName,
}) => {
  return (
    <>
      <nav className="navbar menuBar">
        {onInfo ? (
          <div className="d-flex w-100 align-items-start">
            <FontAwesomeIcon
              icon={faArrowLeft}
              onClick={handleOnInfo}
              className="arrow fa-lg"
            />
            <h5 className="nomeEmpresa ml-3">{enterpriseName}</h5>
          </div>
        ) : (
          <>
            {searchActive ? (
              <form className="d-inline w-100" onSubmit={handleSearch}>
                <div className="form-group input-search">
                  <img
                    src={searchIco}
                    onClick={handleSearchClick}
                    className="searchIco"
                  />
                  <input
                    className="form-control mr-sm-2 inputSearch"
                    type="search"
                    placeholder="Pesquisar"
                    name="search"
                    autoFocus
                  />
                </div>
              </form>
            ) : (
              <>
                <img src={logo} className="d-none d-sm-block" />
                <img
                  src={searchIco}
                  onClick={handleSearchClick}
                  className="searchIco"
                />
              </>
            )}
          </>
        )}
      </nav>
      <div className="container-fluid">
        {enterprises.length > 0 ? (
          /**
           * Se retornar alguma empresa, ele irá carregar o componente Enterprises
           * no lugar do texto informando para ele fazer uma busca ou se não encontrou
           * nenhuma empresa com base no filtro.
           */
          <Enterprises
            enterprises={enterprises}
            handleOnInfo={handleOnInfo}
            onInfo={onInfo}
            getEnterpriseName={getEnterpriseName}
          />
        ) : (
          <div className="align-self-center text-center m-5">
            {loading ? (
              <FontAwesomeIcon
                icon={faCircleNotch}
                spin
                className="fa-5x row mx-auto mt-5 loadingIco"
              />
            ) : firstSearch ? (
              <h4>Clique na busca para iniciar.</h4>
            ) : (
              <h4>Nenhuma empresa foi encontrada para a busca realizada.</h4>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default HomePage;
