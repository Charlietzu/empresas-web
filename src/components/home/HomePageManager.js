import React, { useState } from "react";
import apiInstance from "../../api/empresasApi";
import HomePage from "./HomePage";

/**
 * Componente administrador de estado da página Home
 */
export function HomePageManager() {
  //State para definir as empresas retornadas pela requisição.
  const [enterprises, setEnterprises] = useState();
  //State para definir se a requisição retornou alguma empresa.
  const [enterprisesFound, setEnterprisesFound] = useState(false);
  //State para definir se o usuário está nas informações da empresa.
  const [onInfo, setOnInfo] = useState(false);
  //State para definir se o usuário já realizou sua primeira busca na aplicação.
  const [firstSearch, setFirstSearch] = useState(true);
  //State para definir se o ícone de login será renderizado.
  const [loading, setLoading] = useState(false);
  //State para definir se o usuário está com o input de busca ativo.
  const [searchActive, setSearchActive] = useState(false);
  //State para definir o nome da empresa exibido na navbar.
  const [enterpriseName, setEnterpriseName] = useState("");

  /**
   * Método para fazer a requisição de filtro.
   *
   * Ela define o state loading para verdadeiro caso a requisição esteja sendo feita,
   * se ela concluir ou der erro, o loading é definido para false.
   *
   * Ela define o state "enterprises" e "setEnterprisesFound" dependendo do resultado
   * da requisição. E também define o state "firstSearch" independente do resultado da
   * requisição.
   */
  async function handleSearch(event) {
    event.preventDefault();
    setLoading(true);
    await apiInstance
      .get(`/enterprises?name=${event.target.search.value}`)
      .then((response) => {
        setEnterprises(response.data.enterprises);
        if (response.data.enterprises.length > 0) {
          setEnterprisesFound(true);
        }
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
    setFirstSearch(false);
  }

  /**
   * Método para definir se o usuário está fazendo uma pesquisa ou não.
   *
   * Esse state ativa o input search de pesquisa.
   */
  function handleSearchClick() {
    setSearchActive(!searchActive);
  }

  /**
   * Método para definir se o usuário está página de informações de uma empresa.
   *
   * Esse state ativa o ícone de seta na navbar.
   */
  function handleOnInfo() {
    setOnInfo(!onInfo);
  }

  /**
   * Método para retornar o nome da empresa na navbar quando o usuário
   * clicar em uma empresa.
   * @param {string} enterprise
   */
  function getEnterpriseName(enterpriseName) {
    setEnterpriseName(enterpriseName);
  }

  return (
    <>
      <HomePage
        handleSearch={handleSearch}
        enterprises={enterprisesFound ? enterprises : []}
        handleOnInfo={handleOnInfo}
        onInfo={onInfo}
        firstSearch={firstSearch}
        loading={loading}
        handleSearchClick={handleSearchClick}
        searchActive={searchActive}
        enterpriseName={enterpriseName}
        getEnterpriseName={getEnterpriseName}
      />
    </>
  );
}

export default HomePageManager;
