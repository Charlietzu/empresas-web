import React from "react";
import "./enterprise.css";

/**
 * Componente stateless de detalhes da empresa clicada.
 * @param {state} enterprise
 */
const EnterpriseInfo = ({ enterprise }) => {
  return (
    <>
      <div>
        <div className="card enterpriseCard m-4">
          <div className="card-body">
            <img
              className="card-img-top"
              src={`https://empresas.ioasys.com.br${enterprise.photo}`}
              alt={`E${enterprise.id}`}
            />
            <p className="card-text p-3 cardInfo">{enterprise.description}</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default EnterpriseInfo;
