import React, { useState } from "react";
import "./enterprise.css";
import EnterpriseCard from "./EnterpriseCard";
import EnterpriseInfo from "./EnterpriseInfo";

/**
 * Componente para administração das empresas, no caso,
 * os cards retornados na busca e a página de detalhes da empresa.
 * @param {state} enterprises
 * @param {state} onInfo
 * @param {method} handleOnInfo
 * @param {method} getEnterpriseName
 */
export function Enterprises({
  enterprises,
  handleOnInfo,
  onInfo,
  getEnterpriseName,
}) {
  //State para definir qual empresa foi clicada.
  const [enterprise, setEnterprise] = useState({});

  /**
   * Método para manipular o state com base no card clicado.
   * @param {object} enterprise
   * Este método também chama o método onInfo, que define que o usuário estará
   * vendo as informações detalhadas de uma empresa, ale´m de chamar o método
   * getEnterpriseName, que irá definir o nome da empresa exibido na navbar.
   */
  function handleClick(enterprise) {
    setEnterprise(enterprise);
    handleOnInfo();
    getEnterpriseName(enterprise.enterprise_name);
  }

  /**
   * Se o state "onInfo" for true, o componente retornado será o de detalhes da empresa,
   * passando a empresa clicada através das props.
   *
   * Caso contrário, o usuário estará vendo a iteração das empresas retornadas pelo filtro.
   */
  return onInfo ? (
    <EnterpriseInfo enterprise={enterprise} />
  ) : (
    enterprises.map((enterprise) => (
      <EnterpriseCard
        key={enterprise.id}
        enterprise={enterprise}
        handleClick={handleClick}
      />
    ))
  );
}

export default Enterprises;
