import React from "react";

/**
 * Componente stateless do card de empresa.
 * @param {state} enterprise
 * @param {method} handleClick
 */
const EnterpriseCard = ({ enterprise, handleClick }) => {
  return (
    <div
      className="card enterpriseCard m-4"
      onClick={() => handleClick(enterprise)}
    >
      <div className="card-body">
        <h4 className="card-title">{enterprise.enterprise_name}</h4>
        <h5 className="card-text">
          {enterprise.enterprise_type.enterprise_type_name}
        </h5>
        <p className="card-text">{enterprise.country}</p>
      </div>
    </div>
  );
};

export default EnterpriseCard;
