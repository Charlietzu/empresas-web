import React from "react";
import LoginManager from "./components/login/LoginManager";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import PrivateRoute from "./routes/PrivateRoute";
import HomePageManager from "./components/home/HomePageManager";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={LoginManager} />
        <PrivateRoute exact path="/home" component={HomePageManager} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
