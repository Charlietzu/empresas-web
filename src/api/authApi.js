/**
 * Esse arquivo contém as funcionalidades referentes a autenticação da API.
 * Utilizei a API do Web Storage para armazenar os headers.
 */

/**
 * O access-token/client/uid são os headers retornados pela requisição de login
 * da API utilizada no projeto.
 */
export const ACCESS_TOKEN = "access-token";
export const CLIENT = "client";
export const UID = "uid";

/**
 * Essas 3 funções retornam os headers armazenados no localStorage
 */
export const getAccessToken = () => localStorage.getItem(ACCESS_TOKEN);
export const getClient = () => localStorage.getItem(CLIENT);
export const getUid = () => localStorage.getItem(UID);

/**
 * Essa função define os 3 headers no localStorage.
 * @param {string} access_token
 * @param {string} client
 * @param {string} uid
 */
export const setLogin = (access_token, client, uid) => {
  localStorage.setItem(ACCESS_TOKEN, access_token);
  localStorage.setItem(CLIENT, client);
  localStorage.setItem(UID, uid);
};

/**
 * Essa função verifica se o usuário está logado.
 * Se ele possuir os 3 headers então ela retorna true e o usuário está logado, caso
 * não possua um dos headers ele não estará logado.
 */
export function getLoginHeaders() {
  if (getAccessToken() && getClient() && getUid()) {
    return true;
  } else {
    return false;
  }
}

/**
 * Essa função faz o logout através da remoção dos 3 headers do localStorage.
 */
export const setLogout = () => {
  localStorage.removeItem(ACCESS_TOKEN);
  localStorage.removeItem(CLIENT);
  localStorage.removeItem(UID);
};
