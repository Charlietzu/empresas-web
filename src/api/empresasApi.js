/**
 * Esse arquivo contém a parametrização referente a utilização da API externa.
 * Utilizei a lib axios para fazer as requisições.
 */

import axios from "axios";
import { getAccessToken, getLoginHeaders, getClient, getUid } from "./authApi";

var URL = "https://empresas.ioasys.com.br/api/v1";
let baseURL = URL;

/**
 * Instanciação do axios.
 */
const apiInstance = axios.create({ baseURL: baseURL });

/**
 * Aqui fazemos a interceptação de todas as requests feitas na instância.
 * Se o usuário estiver logado, os 3 headers serão passados para o header
 * da requisição em questão.
 */
apiInstance.interceptors.request.use((config) => {
  let logged = getLoginHeaders();
  if (logged) {
    config.headers["access-token"] = getAccessToken();
    config.headers.client = getClient();
    config.headers.uid = getUid();
  }
  return config;
});

export default apiInstance;
