import React from "react";
import { Route, Redirect } from "react-router-dom";
import { getLoginHeaders } from "../api/authApi";

/**
 * Parametrização das rotas privadas.
 * @param {component} component
 */
function PrivateRoute({ component: Component, ...rest }) {
  /**
   * Se o usuário possuir os 3 headers, ele terá acesso ao componente,
   * caso não, ele será redirecionado para a tela de login.
   */
  return (
    <Route
      {...rest}
      render={(props) =>
        getLoginHeaders() ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
}

export default PrivateRoute;
